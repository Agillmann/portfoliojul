import React, { Component } from 'react';
import APropos from './APropos';

class AProposContainer extends Component {
  /////////////// RENDER
  render() {
    return (
      <div className="">
        <APropos/>
      </div>
    );
  }
}

export default AProposContainer;
