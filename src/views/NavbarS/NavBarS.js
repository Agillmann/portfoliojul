import React from 'react';
import { Link } from 'react-router-dom';

// CSS
import './NavBarS.css';

const NavBar = () => (
      <ul className="d-flex flex-column nav-bar-s">
        <Link className= "" to={`/`} key="home">
          <li>
            Home
          </li>
        </Link>
        <Link className= "" to={`/a-propos`} key="apropo">
          <li>
            A propos
          </li>
        </Link>
        <Link className= "" to={`/portfolio`} key="portfolio">
          <li>
            Portfolio
          </li>
        </Link>
        <Link className= "" to={`/contact`} key="contact">
          <li>
            Contact
          </li>
        </Link>
      </ul>
);

export default NavBar;
