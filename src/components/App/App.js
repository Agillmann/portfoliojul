import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';


// Components
import HomeContainer from '../../views/Home/HomeContainer';
import AProposContainer from '../../views/APropos/AProposContainer';
import PortfolioContainer from '../../views/Portfolio/PortfolioContainer';
import ContactContainer from '../../views/Contact/ContactContainer';

// CSS
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="container-fluid">
        <Router>
          <div>
            <Route exact path="/" component={HomeContainer} />
            <Route path="/a-propos" component={AProposContainer} />
            <Route path="/portfolio" component={PortfolioContainer} />
            <Route path="/contact" component={ContactContainer} />
          </div>
        </Router>

      </div>
    );
  }
}

export default App;
