import React from 'react';
import { Link } from 'react-router-dom';

// CSS
import './Success.css';

const Success = () => (
  <div>
      <h2>Le message a bien été envoyé</h2>
      <ul className="d-flex flex-column nav-bar-s">
        <Link className= "" to={`/`} key="home">
          <li>
            Home
          </li>
        </Link>
        <Link className= "" to={`/a-propos`} key="apropo">
          <li>
            A propos
          </li>
        </Link>
        <Link className= "" to={`/portfolio`} key="portfolio">
          <li>
            Portfolio
          </li>
        </Link>
        <Link className= "" to={`/contact`} key="contact">
          <li>
            Contact
          </li>
        </Link>
      </ul>
  </div>
);

export default Success;
