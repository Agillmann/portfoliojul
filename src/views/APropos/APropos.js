import React from 'react';

// Components
import SideBar from '../SideBar/SideBarContainer';

// CSS
import './APropos.css';

const APropos = () => (
  <div id="apropos-container" className="row d-flex flex-wrap">
    <SideBar/>
    <div id="main-apropos" className="col-md-10 md-offset-2  d-flex flex-column justify-content-start">
      <div id="" className="row d-flex flex-wrap">
        <ul className="navbar-content col-md-12 d-flex justify-content-around">
          <li>Voir Tout</li>
          <li>Qui suis-je ?</li>
          <li>Formation</li>
          <li>Compétences</li>
        </ul>
      </div>
      <div id="qui-suis-je" className="row d-flex flex-wrap">
        <div className="col-md-2">
          <h2 className="text-right"><span className="font-weight-bold text-uppercase">Qui</span><br />suis-je ?</h2>
        </div>
        <div className="col-md-9">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis, diam a accumsan condimentum, odio ante suscipit dolor, vitae tristique dui tellus tincidunt mauris. Nullam aliquam, nisl dignissim porta sollicitudin, odio odio maximus felis, a ullamcorper sem dolor et mauris. Mauris dictum orci vel leo maximus, nec tincidunt urna sollicitudin. Fusce quis nibh sapien. Aliquam id urna pharetra, pellentesque ipsum a, ultrices urna.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis, diam a accumsan condimentum, odio ante suscipit dolor, vitae tristique dui tellus tincidunt mauris. Nullam aliquam, nisl dignissim porta sollicitudin, odio odio maximus felis, a ullamcorper sem dolor et mauris. Mauris dictum orci vel leo maximus, nec tincidunt urna sollicitudin. Fusce quis nibh sapien. Aliquam id urna pharetra, pellentesque ipsum a, ultrices urna.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis, diam a accumsan condimentum, odio ante suscipit dolor, vitae tristique dui tellus tincidunt mauris. Nullam aliquam, nisl dignissim porta sollicitudin, odio odio maximus felis, a ullamcorper sem dolor et mauris. Mauris dictum orci vel leo maximus, nec tincidunt urna sollicitudin.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mattis, diam a accumsan condimentum, odio ante suscipit dolor, vitae tristique dui tellus tincidunt mauris. Nullam aliquam, nisl dignissim porta sollicitudin, odio odio maximus felis, a ullamcorper sem dolor et mauris. Mauris dictum orci vel leo maximus, nec tincidunt urna sollicitudin. Fusce quis nibh sapien. Aliquam id urna pharetra, pellentesque ipsum a, ultrices urna.</p>
        </div>
      </div>
      <div className="row d-flex flex-wrap ">
        <div id="collaborer" className="col-md-10 offset-md-1 d-flex flex-column justify-content-between">
          <h2 className="font-weight-bold mb-4">Vous souhaitez collaborer ?</h2>
          <p>Vous pouvez me contacter a l'adresse suivante, pour toute demande de renseignement, collaboration ou devis<br/> personnalisé.</p>
          <h2>ruocco.julia@icloud.com</h2>
        </div>
      </div>
      <div id="formation-content" className="row d-flex flex-wrap">
        <div className="col-md-2">
          <h2 className="text-right"><span className="font-weight-bold text-uppercase">Ma</span><br />formation</h2>
        </div>
        <div className="col-md-9">
          <p>
            <span className="font-weight-bold">Baccalauréat Scientifique - 2017</span><br/><span className="font-italic">Option Sciences de la Vie et de la Terre</span><br/>Mon baccalauréat ma permise d'acquérir une certaine méthode de travail, et beaucoup de connaissances sur le monde du vivant. Les sciences m'intéressent beaucoup, je me documente encore à ce sujet.
          </p>
          <p>
            <span className="font-weight-bold">Première année à l'EEMI - 2018</span><br/><span className="font-italic">Fastrack (programme d'un an adapté sur 5 mois)</span><br/>Cette première année en rentrée décalée fut intense : j'ai appris beaucoup de choses concernant le webmarketing, le web développement et le web design. Cela m'a demandé beaucoup de travail et de concentration, je ne regrette pas cette experience qui a été très enrichissante.
          </p>
          <p>
            <span className="font-weight-bold">Deuxième année à l'EEMI - 2019</span><br/><span className="font-italic">Spécialité Web Design</span><br/>J'ai décidé de me spécialiser dans le web design, pour apprendre d'avantage sur le domaine qui ma toujours plu. Durant cette deuxième année, j'ai enrichit mes compétences en design et ma capacité à travailler en groupe, notamment grâce au Design Thinking, aux méthodes agiles ..
          </p>
        </div>
      </div>
      <div id="competences" className="row d-flex flex-wrap">
        <div className="col-md-2">
          <h2 className="text-right"><span className="font-weight-bold text-uppercase">Mes</span><br />compétences</h2>
        </div>
        <div className="col-md-7">
          <div id="" className="row d-flex">
            <div className="col-md-1 competences-content">
              <img src="https://res.cloudinary.com/gillmann/image/upload/v1547918669/portfolio-julia/computer-4.png" alt=""/>
            </div>
            <div className="col-md-10 offset-md-1">
              <p>
                Suite Adobe (Photoshop, Illustrator, XD, After Effect .. )<br/>
                Figma et Sketch<br/>
                Wordpress et Wix<br/>
              </p>
            </div>
            <div className="col-md-1 competences-content">
              <img src="https://res.cloudinary.com/gillmann/image/upload/v1547918669/portfolio-julia/html.png" alt=""/>
            </div>
            <div className="col-md-10 offset-md-1">
              <p>
                HTML et CSS<br/>
                Bootstrap<br/>
                Bootstrap Studio<br/>
              </p>
            </div>
            <div className="col-md-1 competences-content">
              <img src="https://res.cloudinary.com/gillmann/image/upload/v1547918670/portfolio-julia/mind.png" alt=""/>
            </div>
            <div className="col-md-10 offset-md-1">
              <p>
                Design Thinking<br/>
                Méthodes Agiles<br/>
                Business Model et Droit du Web<br/>
              </p>
            </div>
            <div className="col-md-1 competences-content">
              <img src="https://res.cloudinary.com/gillmann/image/upload/v1547918670/portfolio-julia/speak.png" alt=""/>
            </div>
            <div className="col-md-10 offset-md-1">
              <p>
                Français (langue maternelle)<br/>
                Anglais (niveau professionnel)<br/>
                Espagnol (niveau scolaire)<br/>
              </p>
            </div>
          </div>
        </div>
        <div id="savoir-plus" className="col-md-2 d-flex flex-column justify-content-between">
          <h2 className="font-weight-bold">Vous voulez en savoir plus ?</h2>
          <p>Télécharger mon CV en format Pdf ci-dessous. Ou bien, envoyer un mail à l'adresse suivante : ruocco.julia@icloud.com</p>
          <a href="" download="CV" target="_blank" className="btn btn-light">Télécharger</a>
        </div>
      </div>
    </div>
  </div>
);

export default APropos;
