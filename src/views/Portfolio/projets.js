/**
* Array(tag, titre, date, url, img)
*
*/

class projets {
  projets = [
    {"key":1, "tag": "site-web", "titre":"Titre du projet", "date":"Jan-2019", "url": "", "img": "https://res.cloudinary.com/gillmann/image/upload/v1548467211/portfolio-julia/webapi-player-example.jpg"},
    {"key":2, "tag": "print-affichage", "titre":"Titre du projet", "date":"Jan-2019", "url": "", "img": "https://res.cloudinary.com/gillmann/image/upload/e_auto_brightness,q_auto,r_0/a_0/v1548468497/portfolio-julia/concreative.png"},
    {"key":3, "tag": "logos", "titre":"Titre du projet", "date":"Jan-2019", "url": "", "img": ""},
    {"key":4, "tag": "motion", "titre":"Titre du projet", "date":"Jan-2019", "url": "", "img": ""},
    {"key":5, "tag": "autres", "titre":"Titre du projet", "date":"Jan-2019", "url": "", "img": ""}
  ]
  get(){
    return this.projets;
  }
}

export default new projets();
