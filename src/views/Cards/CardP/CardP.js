import React from 'react';

// Components

// CSS
import './CardP.css';

const CardP = ({title, tag, img, url}) => (

      <div className="col-md-12 d-flex justify-content-between flex-column mb-2">
        <a href={url} target="_blank" rel="noopener noreferrer">
          <img src={img} alt=""/>
          <h3 className="mt-3 ">{title}</h3>
          <h4 className="mt-2">{tag}</h4>
        </a>
      </div>

);

export default CardP;
