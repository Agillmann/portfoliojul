import React, { Component } from 'react';
import SideBar from './SideBar';

class SideBarContainer extends Component {
  /////////////// RENDER
  render() {
    return (
      <SideBar/>
    );
  }
}

export default SideBarContainer;
