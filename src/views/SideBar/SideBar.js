import React from 'react';

// Components
import NavBarS from '../NavbarS/NavBarSContainer';

// CSS
import './SideBar.css';

const SideBar = () => (
  <div className="col-md-2 d-flex justify-content-start flex-column">
    <div className="sidebar-content">
      <img id="logo" className="" src="https://res.cloudinary.com/gillmann/image/upload/v1547918662/portfolio-julia/logo.png" alt="logo Julia Ruocco"/>
      <NavBarS/>
      <div className="container-rs">
        <a href="https://www.linkedin.com/in/julia-ruocco-809983159" target="_blank" rel="noopener noreferrer">
          <img className="logo-rs" src="https://res.cloudinary.com/gillmann/image/upload/v1547918670/portfolio-julia/instagram.png" alt="logo instagram"/>
        </a>
        <a href="https://www.linkedin.com/in/julia-ruocco-809983159" target="_blank" rel="noopener noreferrer">
          <img className="logo-rs" src="https://res.cloudinary.com/gillmann/image/upload/v1547918669/portfolio-julia/dribbble.png" alt="logo dribble"/>
        </a>
        <a href="https://www.linkedin.com/in/julia-ruocco-809983159" target="_blank" rel="noopener noreferrer">
          <img className="logo-rs" src="https://res.cloudinary.com/gillmann/image/upload/v1547918670/portfolio-julia/linkedin.png" alt="logo linkedin"/>
        </a>
        <p className="text-center">©2019 Julia Ruocco</p>
      </div>
      </div>
  </div>
);

export default SideBar;
