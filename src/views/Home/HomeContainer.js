import React, { Component } from 'react';
import Home from './Home';

class HomeContainer extends Component {
  /////////////// RENDER
  render() {
    return (
      <div className="">
        <Home/>
      </div>
    );
  }
}

export default HomeContainer;
