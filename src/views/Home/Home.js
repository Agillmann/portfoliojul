import React from 'react';

// Components
import SideBar from '../SideBar/SideBarContainer';

// CSS
import './Home.css';

const Home = () => (
  <div id="home-container" className="row d-flex flex-wrap">
    <SideBar/>
    <div id="main-background" className="col-md-10 col-md-offset-2 d-flex flex-column justify-content-center">
      <div className="d-flex align-items-center flex-column">
        <img id="bienvenue" className="justify-content-center " src="https://res.cloudinary.com/gillmann/image/upload/v1547918662/portfolio-julia/bienvenue.png" alt="bienvenue"/>
        <h2 id="home-text" className="text-center">WEBDESIGN</h2>
      </div>
    </div>
  </div>
);

export default Home;
