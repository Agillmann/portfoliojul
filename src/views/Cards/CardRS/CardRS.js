import React from 'react';

// Components
import SideBar from '../SideBar/SideBarContainer';

// CSS
import './CardRS.css';

const CardRS = () => (
  <div id="home-container" className="row d-flex flex-wrap">
    <SideBar/>
    <div id="main-contact" className="col-md-10 d-flex flex-column justify-content-center">
      <p className="text-center">CardRS is coming</p>
    </div>
  </div>
);

export default CardRS;
