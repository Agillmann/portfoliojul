import React, { Component } from 'react';
import Portfolio from './Portfolio';

// import NavBarP from '../NavbarP/NavBarPContainer';
import SideBar from '../SideBar/SideBarContainer';

import projets from './projets';
class PortfolioContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tag: "all",
    };
  }

  projetsFilter = (tag) => {
    var projetsFilted = [];
    if (tag !== "all") {
      for(var i = 0; i < projets.get().length; i++){
        if (projets.get()[i].tag === tag) {
          projetsFilted.push(projets.get()[i])
        }
      }
      return projetsFilted
    }
    return projets.get()
  }

  handleAll = (tag) => {
    this.setState({
      tag:tag
    });
  }

  /////////////// RENDER
  render() {
    const projetsFilted = this.projetsFilter(this.state.tag)
    // console.log(projets.get);
    return (
      <div className="row d-flex flex-wrap">
        <SideBar />
        <div id="main-contact" className="home-container col-md-10 col-md-offset-2 d-flex flex-column justify-content-start">
          <ul className="navbar-content d-flex justify-content-around">
            <li onClick={() => this.handleAll("all")}>Voir Tout</li>
            <li onClick={() => this.handleAll("site-web")}>Site Web</li>
            <li onClick={() => this.handleAll("print-affichage")}>Print / Affichages</li>
            <li onClick={() => this.handleAll("logos")}>Logos</li>
            <li onClick={() => this.handleAll("motion")}>Motion Design</li>
            <li onClick={() => this.handleAll("autres")}>Autres</li>
          </ul>
          <Portfolio projets={projetsFilted}/>
        </div>
      </div>
    );
  }
}

export default PortfolioContainer;
