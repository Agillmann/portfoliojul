import React, { Component } from 'react';
import CardP from './CardP';

class CardPContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tag: "",
      titre:"",
      date: new Date()
    };

  }
  /////////////// RENDER
  render() {
    return (
      <div id="card-container" className="mb-2 ml-3">
        <CardP
          title = {this.props.title}
          tag = {this.props.tag}
          img = {this.props.img}
          url = {this.props.url}
        />
      </div>
    );
  }
}

export default CardPContainer;
