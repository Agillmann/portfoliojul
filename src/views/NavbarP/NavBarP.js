import React from 'react';
import { Link } from 'react-router-dom';

// CSS
import './NavBarP.css';

const NavBarP = () => (
      <ul className="navbar-content d-flex justify-content-around">
        <li>Voir Tout</li>
        <li>Site Web</li>
        <li>Print / Affichages</li>
        <li>Logos</li>
        <li>Motion Design</li>
        <li>Autres</li>
      </ul>
);

export default NavBarP;
