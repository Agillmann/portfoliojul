import React from 'react';

// Components
import SideBar from '../SideBar/SideBarContainer';

// CSS
import './Contact.css';

const Contact = () => (
  <div className=" row d-flex flex-wrap">
    <SideBar/>
    <div id="main-contact" className="home-container col-md-10 d-flex flex-column">
      <div className="row">
        <div className="col-md-5 offset-md-1">
          <h2>Me <span>trouver</span></h2>
          <div className="row me-trouver">
            <div className="col-md-12 d-flex align-self-center">
              <a href="" target="_blank">
                <img className="contact-logo" src="https://res.cloudinary.com/gillmann/image/upload/v1547918670/portfolio-julia/mail.png"/>
              </a>
              <p className=""><span className="font-weight-bold">EMAIL</span><br/>ruocco.julia@icloud.com</p>
            </div>
          </div>
          <div className="row me-trouver">
            <div className="col-md-12 d-flex align-self-center">
              <a href="" target="_blank">
                <img className="contact-logo" src="https://res.cloudinary.com/gillmann/image/upload/v1547918670/portfolio-julia/instagram.png"/>
              </a>
              <p className=""><span className="font-weight-bold">INSTAGRAM</span><br/>@julia_design_me</p>
            </div>
          </div>
          <div className="row me-trouver">
            <div className="col-md-12 d-flex align-self-center">
              <a href="" target="_blank">
                <img className="contact-logo" src="https://res.cloudinary.com/gillmann/image/upload/v1547918670/portfolio-julia/linkedin.png"/>
              </a>
              <p className=""><span className="font-weight-bold">LINKEDIN</span><br/>Julia Ruocco</p>
            </div>
          </div>
          <div className="row me-trouver">
            <div className="col-md-12 d-flex align-self-center">
              <a href="" target="_blank">
                <img className="contact-logo" src="https://res.cloudinary.com/gillmann/image/upload/v1547918669/portfolio-julia/dribbble.png"/>
              </a>
              <p className=""><span className="font-weight-bold">DRIBBLE</span><br/>@julia_design_me</p>
            </div>
          </div>
        </div>
        <div className="col-md-5">
          <h2>Me <span>contacter</span></h2>
          <div className="row">
            <div className="col-md-12">
              <form name="sentMessage" method="post" data-netlify="true">
                <input type="hidden" name="form-name" value="sentMessage"/>
                <div class="form-group">
                  <input type="text" class="form-control" name="nom" id="nom" placeholder="Nom *"/>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="E-mail *"/>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="sujet" id="sujet" placeholder="Sujet *"/>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="msg" id="msg" rows="10" placeholder="Message *"></textarea>
                </div>
                <button type="submit" class="btn btn-info">ENVOYER</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default Contact;
