import React from 'react';

// Components

import CardP from '../Cards/CardP/CardPContainer';


// CSS
import './Portfolio.css';
const Portfolio = ({projets}) => (
    <div className="row d-flex  justify-content-baseline">
      {projets.map((projets) => (
        <CardP
          title = {projets.titre}
          tag = {projets.tag}
          img = {projets.img}
          url = {projets.url}
          key = {projets.key}
        />
      ))}
    </div>
);

export default Portfolio;
